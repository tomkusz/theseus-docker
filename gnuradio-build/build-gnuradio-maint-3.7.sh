#!/bin/sh

function throw_help_msg {
    echo 'usage: $0 image-name uhd-base-image gnuradio-branch'
    echo '  - image-name: Output image name to use when building [defaults to "gnuradio:maint-3.7"]'
    echo '  - uhd-base-image: UHD base docker image to build on'
    echo
    echo Examples:
    echo $0
    echo $0 maint-3.7
    echo $0 v3.13.0.1
    exit
}

# Check for help string
if [ "$1" == "-h" ] || [ "$1" == "help" ] ||
   [ "$2" == "-h" ] || [ "$2" == "help" ]; then
    throw_help_msg
fi

# Default tag to "uhd"
if [ -z "$1" ]; then
    DOCKER_TAG="gnuradio:maint-3.7"
else
    DOCKER_TAG=$1
fi

# Default branch to master
if [ -z "$2" ]; then
    UHD_BASE_IMAGE="registry.gitlab.com/ejk/rf-community-docker/uhd:master-rfnoc-all"
else
    UHD_BASE_IMAGE=$2
fi

docker build -t $DOCKER_TAG \
    --build-arg UHD_BASE_IMAGE=$UHD_BASE_IMAGE \
    .
